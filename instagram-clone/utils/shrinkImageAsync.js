import { ImageManipulator } from "expo";

async function reduceImageAsync(uri) {
  return await ImageManipulator.manipulateAsync(
    uri,
    [{ resize: { width: 500 } }],
    {
      compress: 0.5
    }
  );
}

export default reduceImageAsync;
